package com.prashant.kafka.producer;

/**
 * Created by prashant.sabnekar on 7/5/2017.
 */
public interface ProducerRecordCallback {
    void messageProduced();
}

package com.prashant.kafka.producer;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;

import java.util.Properties;

/**
 * Created by prashant.sabnekar on 7/5/2017.
 */
public class SimpleProducer {

    /**
     * author prashant sabnekar
     */
    public void demo() {
        String topic = "prashant";

        Properties props = new Properties();
        props.put("bootstrap.servers", "localhost:9092");
        props.put("acks", "all");
        props.put("retries", 0);
        props.put("batch.size", 16384);
        props.put("linger.ms", 1);
        props.put("buffer.memory", 33554433);
        props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");

        Producer<String, String> producer = new KafkaProducer<String, String>(props);

        for(int i=0; i<10; ++i) {
            producer.send(new ProducerRecord<String, String>(topic, Integer.toString(i), Integer.toString(i)), ((RecordMetadata var1, Exception var2) -> {
                System.out.println("Messages sent successfully");
            }));

        }
        producer.close();
    }

    public static void main(String args[]) {
        SimpleProducer sp = new SimpleProducer();
        sp.demo();

    }
}

package com.prashant.kafka.producer;

/**
 * Created by prashant.sabnekar on 7/5/2017.
 */
public class ProducerRecordCallbackImpl implements ProducerRecordCallback {
    public void messageProduced() {
        System.out.println("Message produced");
    }
}
